<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Delete</name>
   <tag></tag>
   <elementGuidId>77787b51-2bdc-4660-bb91-1da911251060</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.added-manually</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@onclick='deleteElement()']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>33cdba26-5b98-450b-ae8f-a5710a09ac1f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>added-manually</value>
      <webElementGuid>d692cee8-5239-4f8b-ba14-4e7add87089b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>deleteElement()</value>
      <webElementGuid>fcfabe04-5681-48b3-b525-7dfa7d2cb0a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Delete</value>
      <webElementGuid>6fd2cea8-9c91-412f-849e-6877452691e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;elements&quot;)/button[@class=&quot;added-manually&quot;]</value>
      <webElementGuid>c1590377-4676-4e66-88e9-6528225b5173</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@onclick='deleteElement()']</value>
      <webElementGuid>f74b282a-9bca-4687-b126-0c9dd8c9bff0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='elements']/button</value>
      <webElementGuid>dfc4f25e-b646-4ed7-99b2-817d99eee82f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Element'])[1]/following::button[1]</value>
      <webElementGuid>62e27075-8b62-4c48-923a-22c1e94a770c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add/Remove Elements'])[1]/following::button[2]</value>
      <webElementGuid>bfd80c33-62a0-49e7-87c3-5c397d9b33d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Elemental Selenium'])[1]/preceding::button[1]</value>
      <webElementGuid>e1c86e27-e544-44f8-a708-d62cd6123174</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Delete']/parent::*</value>
      <webElementGuid>ace36168-03eb-4ffe-914a-34dfff098e8f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/button</value>
      <webElementGuid>80d5baf6-f398-4732-b432-d25eb7c3b672</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Delete' or . = 'Delete')]</value>
      <webElementGuid>8f67b7c3-081e-4a24-b84a-2d4cedd105fc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
